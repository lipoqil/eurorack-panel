# Eurorack panel

Scad file's code forked from https://www.thingiverse.com/thing:2393436

This fork adds ability to specify Us, so by changing _uCount_ you can get pannel of height you need. Of course by
changing _panelHp_ you'll get the width you need.

## Background

- https://en.wikipedia.org/wiki/Eurorack
- https://www.sweetwater.com/store/search.php?s=eurorack+blank+panels
